<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApagarProduto extends CI_Controller {

//Função Apagar registro
	public function apagar($codigo=NULL){

		//Verifica se esta logado
			if(isset($_SESSION['usuario'])){

				//Verifica se foi passado um ID, se não vai para a página listar produtos
				if($codigo == NULL) {
					redirect("http://localhost/lojaroupa/ci/index.php/listagemproduto/listaprodutos");
				}

				//Carrega o Model Produtos				
				$this->load->model('lista_model','lista');

				//Faz a consulta no banco de dados pra verificar se existe
				$query = $this->lista->getProdutoByEstoque($codigo);

				//Verifica se foi encontrado um registro com a ID passada
				if($query != NULL) {
					
					//Executa a função apagarProdutos do produtos_model
					$this->lista->apagarProduto($query->id);
					//redirect('http://localhost/monitoria/ci/index.php/listadealunosapagar');

				} else {
					//Se não encontrou nenhum registro no banco de dados com a ID passada ele volta para página listar alunos
					redirect("http://localhost/lojaroupa/ci/index.php/listagemproduto/listaprodutos");
				}

				//Fazemos um redicionamento para a mesma página
						//Carregar Model
						$this->load->model("lista_model","lista");

						//Buscar dados no banco
						$produto['itens'] = $this->lista->getProdutos();

						$this->load->view('listaprodutoapagar', $produto);
			
		}else{
			redirect('http://localhost/lojaroupa/ci');
		}
	}
}	