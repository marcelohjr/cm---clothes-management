<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdicionarProduto extends CI_Controller {

public function add(){
		//Carrega o Model Monitoria				
		$this->load->model('loja_model', 'loja');
		//Verifica se esta logado
		if(isset($_SESSION['usuario'])){
			//Carrega a View
			$this->load->view('adicionarestoque');
		}else{
			redirect('http://localhost/lojaroupa/ci');
		}
	}
}