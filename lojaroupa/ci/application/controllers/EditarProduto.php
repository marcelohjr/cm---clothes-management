<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EditarProduto extends CI_Controller {
	
	//Página com formulário para editar produto
	public function editar($codigo=NULL){

		//Verifica se esta logado
		if(isset($_SESSION['usuario'])){
			//Verifica se foi passado um ID, se não vai para a página listar produtos
			if($codigo == NULL) {
				redirect('http://localhost/lojaroupa/ci');
			}

			//Carrega o Model Produtos				
			$this->load->model('lista_model', 'lista');

			//Faz a consulta no banco de dados pra verificar se existe
			$query = $this->lista->getProdutoByEstoque($codigo);

			//Verifica que a consulta voltar um registro, se não vai para a página listar produtos
			if($query == NULL) {
				redirect('http://localhost/lojaroupa/ci');
			}
			
			//Criamos uma array onde vai guardar os dados do aluno e passamos como parametro para view;	
			$produtos['produto'] = $query;

			//Carrega a View
			$this->load->view('atualizarestoque', $produtos);
		}else{
			redirect('http://localhost/lojaroupa/ci');
		}	
	}
}