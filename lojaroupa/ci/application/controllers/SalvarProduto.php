<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SalvarProduto extends CI_Controller {


//Função salvar no DB
	public function salvar(){
		//Verifica se esta logado
		if(isset($_SESSION['usuario'])){
				//Verifica se foi passado o campo nome vazio.
				if ($this->input->post('nome') == NULL) {
					echo 'O nome do produto é obrigatório.';
					echo '<a href="http://localhost/lojaroupa/ci/index.php/adicionarproduto/add" title="voltar">Voltar</a>';
				} else{
					if ($this->input->post('preco_compra') == NULL) {
						echo 'O precode compra do produto é obrigatório.';
						echo '<a href="http://localhost/lojaroupa/ci/index.php/adicionarproduto/add" title="voltar">Voltar</a>';
					} else {
						//Carrega o Model Produtos				
						$this->load->model('lista_model', 'lista');
						//Pega dados do post e guarda na array $dados
						$dados['id'] = $this->input->post('codigo');
						$dados['nome'] = $this->input->post('nome');
						$dados['preco_compra'] = $this->input->post('preco_compra');
						$dados['preco_venda'] = $this->input->post('preco_venda');		
						$dados['quantidade'] = $this->input->post('quantidade');
						$dados['tipo'] = $this->input->post('tipo');
						$dados['tamanho'] = $this->input->post('tamanho');
						

						$this->lista->addProduto($dados);

						//Fazemos um redicionamento para a página 		
						redirect("http://localhost/lojaroupa/ci/index.php/listagemproduto/listaprodutos");	
					}
				}
		}else{
			redirect('http://localhost/lojaroupa/ci');
		}
	}
}