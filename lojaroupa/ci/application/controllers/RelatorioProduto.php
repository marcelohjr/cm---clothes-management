<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RelatorioProduto extends CI_Controller {

//Página com a lista de editar aluno
	public function paginaRelatorio(){
		
			//Carregar Model
			$this->load->model("lista_model","lista");

			//Verifica se esta logado
			if(isset($_SESSION['usuario'])){
				//Buscar dados no banco
				$produto['itens'] = $this->lista->getProdutos();

				//Passar dados do banco para view
				$this->load->view('relatorioestoque', $produto);
			}else{
				redirect('http://localhost/lojaroupa/ci');
			}

	}
}