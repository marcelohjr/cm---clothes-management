<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AtualizarProduto extends CI_Controller {

public function atualizar(){
		//Verifica se esta logado
		if(isset($_SESSION['usuario'])){
				//Verifica se foi passado o campo nome vazio.
				if ($this->input->post('nome') == NULL) {
					echo 'O nome do produto é obrigatório.';
					echo '<a href="http://localhost/lojaroupa/ci/index.php/listagemproduto/listaprodutos" title="voltar">Voltar</a>';
				} else{
					if ($this->input->post('tipo') == NULL) {
						echo 'o tipo do produto é obrigatório.';
						echo '<a href="http://localhost/lojaroupa/ci/index.php/listagemproduto/listaprodutos" title="voltar">Voltar</a>';
					} else {
						//Carrega o Model Produtos				
						$this->load->model('lista_model', 'lista');
						
						//Pega dados do post e guarda na array $dados
						$dados['id'] = $this->input->post('codigo');
						$dados['nome'] = $this->input->post('nome');
						$dados['preco_compra'] = $this->input->post('preco_compra');
						$dados['preco_venda'] = $this->input->post('preco_venda');		
						$dados['quantidade'] = $this->input->post('quantidade');
						$dados['tipo'] = $this->input->post('tipo');
						$dados['tamanho'] = $this->input->post('tamanho');		
						
						//Verifica se foi passado via post a id do produtos
					if ($this->input->post('codigo') != NULL) {		
						//Se foi passado ele vai fazer atualização no registro.	
						$this->lista->editarProduto($dados, $this->input->post('codigo'));

					} else {
						//Se Não foi passado id ele adiciona um novo registro
						$this->lista->addProduto($dados);
					}
						//Fazemos um redicionamento para a mesma página
						//Carregar Model
						$this->load->model("lista_model","lista");

						//Buscar dados no banco
						$produto['itens'] = $this->lista->getProdutos();

						$this->load->view('listaprodutoatualizar', $produto);
						//redirect("http://localhost/monitoria/ci/index.php/listadealunosatualizar");	
						//Colocar uma caixa javascript de confirmaçãos
					}
				}
		}else{
			//Fazemos um redicionamento para a página 		
			redirect("http://localhost/monitoria/ci");
		}
	}
}