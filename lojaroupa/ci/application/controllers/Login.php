<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	
	public function index()
	{
		//Carrega o Model Login				
		$this->load->model('loja_model', 'loja');
		//Verifica se está logado
		if(isset($_SESSION['usuario'])){
			$this->load->view('login');
		}else{					
			//Carrega a View
			$this->load->view('login');
		}
	}

	public function entrarlogin(){
			
			$this->load->view('login');

			$login = $this->input->post('login');
	        $senha = $this->input->post('password');

	        //Verifica se a ID no banco de dados
	        $this->db->where('nome', $login);
	        $this->db->where('senha', $senha);
			
			//Pega os dados no banco de dados
	        $query = $this->db->get('usuario');

	        if( $query->num_rows() == 1){
					$usuario = $query->row();
					$_SESSION['usuario'] = $usuario->nome;
					/*
					//Carrega o Model Produtos				
					$this->load->model('monitoria_model', 'monitoria');
					//array para verificar senha
					$pessoa['monitoria'] = $this->monitoria->getAlunos();
					//carrega view
					$this->load->view('listadealunos',$pessoa);
					*/
					//$this->load->view('estoque');

					redirect('http://localhost/lojaroupa/ci/index.php/listagemproduto/listaprodutos');
				}else{
					echo '<script>
	                   	alert("Login ou Senha incorretos");
	                 </script>';
				}
			
	}

	public function logout(){

			unset($_SESSION['usuario']);
			//$this->session->destroy();
			//$this->session->unset_userlogin('is_logged_in');
			//$this->session->unset_userdata('logged');
			//header('location: login.php');
		    //Fazemos um redicionamento para a página 		
			//redirect("http://localhost/monitoria/ci");

			$this->load->model('lista_model', 'lista');					
			//Carrega a View
			$this->load->view('login');
	}

}
