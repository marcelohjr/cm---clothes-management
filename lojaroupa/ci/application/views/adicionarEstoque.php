<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta name="description" content="Lista de alunos da tabela alunos">
    
    <title>Novo produto</title>

     <!-- Bootstrap Core CSS -->
    <link href="/lojaroupa/ci/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/lojaroupa/ci/bootstrap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/lojaroupa/ci/bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/lojaroupa/ci/bootstrap/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/lojaroupa/ci/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body> 

      <div id="wrapper">
         <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://localhost/lojaroupa/ci/index.php/listagemproduto/listaprodutos">CM - Clothes Management</a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li> -->
                        <li class="divider"></li>
                        <li><a href="http://localhost/lojaroupa/ci/index.php/login/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       <br>
                        </li>
                        <li>
                            <a href="http://localhost/lojaroupa/ci/index.php/listagemproduto/listaprodutos"><i class="fa fa-dashboard fa-fw"></i>Início</a>
                        </li>
                        <li>
                            <a href="http://localhost/lojaroupa/ci/index.php/adicionarproduto/add"><i class="fa fa-edit fa-fw"></i>Adicionar Produto </a>
                        </li>
                        <li>
                          
                            <a href="http://localhost/lojaroupa/ci/index.php/paginaeditarproduto/paginaeditar" title="atualizar cadastro"><i class="fa fa fa-edit fa-fw"></i> Atualizar Estoque</a>
                            
                        </li>
                        <li>
                              <a href="http://localhost/lojaroupa/ci/index.php/paginaapagarproduto/paginaapagar"><i class="fa fa-edit fa-fw"></i> Apagar Produto</a>
                        </li>
                        <!--
                        <li>
                            <a href="http://localhost/monitoria/ci/index.php/monitoria/paginaFrequencia"><i class="fa fa-edit fa-fw"></i> Frequência</a>
                        </li>
                        -->
                        <li>
                            <a href="http://localhost/lojaroupa/ci/index.php/relatorioproduto/paginaRelatorio"><i class="fa fa-edit fa-fw"></i> Relatórios</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
                
        <div id="page-wrapper">
          <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Novo Produto</h1>
            </div> 
            <!-- Formulário de novo cadastro  -->
            <form action="http://localhost/lojaroupa/ci/index.php/salvarproduto/salvar" name="form_add" method="post">
              
              <!-- Input text Matricula de alunos -->
              <!--
              <div class="row">
                <div class="col-md-8">
                  <label>Código</label>
                  <input type="text" name="codigo" placeholder=" Digite o codigo do produto" value="" class="form-control">
                </div>

              </div> --> <!-- fim input text matricula de alunos -->
              <!--
              <div class="form-group">
                <label>Disciplina</label>
                    <select name="disciplina" class="form-control" style="width: 20%;">
                      <option value="Estrutura de Dados" >Estrutura de Dados</option>
                     <option value="Banco de Dados" >Banco de Dados</option>
                    </select>
              </div> -->

              
              <!-- Input text Disciplna de Alunos -->
              <!--
              <div class="row">
                <div class="col-md-8">
                  <label>Disciplina</label>
                  <input type="text" name="disciplina" value="" class="form-control">
                </div>
              </div> --><!-- fim input text disciplina de alunos -->
              
              <!-- Input text Nome de Alunos -->
              <div class="row">
                <div class="col-md-8">
                  <label>Nome</label>
                  <input type="text" name="nome" placeholder="Digite o nome da produto" value="" class="form-control">
                </div>
              </div><!-- fim input text nome de alunos -->

              <!-- Input text Email de Alunos -->
              <div class="row">
                <div class="col-md-8">
                  <label>Preço de compra</label>
                  <input type="text" name="preco_compra" placeholder="Digite o preço de compra" value="" class="form-control">
                </div>
              </div><!-- fim input text email de alunos -->

               <div class="row">
                <div class="col-md-8">
                  <label>Preço de venda</label>
                  <input type="text" name="preco_venda" placeholder="Digite o preço de venda" value="" class="form-control">
                </div>
              </div><!-- fim input text email de alunos -->

               <div class="row">
                <div class="col-md-8">
                  <label>Quantidade</label>
                  <input type="text" name="quantidade" placeholder="Digite a quantidade do produto" value="" class="form-control">
                </div>
              </div><!-- fim input text email de alunos -->

              <div class="row">
                <div class="col-md-8">
                  <label>Tipo</label>
                  <input type="text" name="tipo" placeholder="Digite o tipo do produto" value="" class="form-control">
                </div>
              </div><!-- fim input text email de alunos -->

              <div class="row">
                <div class="col-md-8">
                  <label>Tamanho</label>
                  <input type="text" name="tamanho" placeholder="Digite a tamanho do produto" value="" class="form-control">
                </div>
              </div><!-- fim input text email de alunos -->
              
              <!-- Select Aluno ativo ou inativo -->
              <!--
              <div class="row">
                <div class="col-md-2">
                  <label>Ativo</label>
                  <select name="ativo" class="form-control">
                    <option value="presente">Sim</option>
                    <option value="0">Não</option>
                  </select>
                </div>
              </div>
              -->
              <!-- fim select produtos ativo ou inativo -->

              <input type="hidden" name="presenca" value="0">
              <input type="hidden" name="ausencia" value="0">

              <!-- Button submit(enviar) formulário -->
              <br />
              <div class="row">
                <div class="col-md-2">
                  <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
              </div><!-- fim do button submit(enviar) formulário -->
              

            </form><!--Fim formulário de novo cadastro  -->
          </div>
        </div>
      </div>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

  </body>
</html>