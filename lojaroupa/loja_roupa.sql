-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 27-Jun-2019 às 15:20
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loja_roupa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `autenticacao`
--

CREATE TABLE `autenticacao` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 NOT NULL,
  `expired_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `autenticacao`
--

INSERT INTO `autenticacao` (`id`, `usuario_id`, `token`, `expired_at`, `created_at`, `update_at`) VALUES
(1, 1, '$1$.k5bz0za$R65abjVxvb8Ed9e140ZS2/', '2019-06-28 08:02:58', '2019-06-27 02:45:49', '2019-06-27 20:02:58'),
(2, 1, '$1$RxEOohdW$KMhXh6X0Y4Xt7mYOsxoX90', '2019-06-28 00:48:44', '2019-06-27 02:48:44', '2019-06-27 02:48:44'),
(3, 1, '$1$UfnElP3l$lmYyhGQtgALAU6IgXprbA.', '2019-06-28 00:49:08', '2019-06-27 02:49:08', '2019-06-27 02:49:08'),
(4, 1, '$1$kjiy6RxC$UBeHtSazxDj1Z1PhnRqF2.', '2019-06-28 00:55:26', '2019-06-27 02:55:26', '2019-06-27 02:55:26'),
(5, 1, '$1$1s0m9ApR$39eNi7YR5YAy4lWc0GjUp/', '2019-06-28 00:57:40', '2019-06-27 02:57:40', '2019-06-27 02:57:40'),
(6, 1, '$1$cNaCR.i1$9wRKP048xG2UKtyt269FC1', '2019-06-28 01:43:00', '2019-06-27 03:43:00', '2019-06-27 03:43:00'),
(7, 1, '$1$NVA.jHNo$FP1sXCZ7mqCWnpt5zLJNx1', '2019-06-28 01:49:33', '2019-06-27 03:49:33', '2019-06-27 03:49:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estoque`
--

CREATE TABLE `estoque` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) CHARACTER SET utf8 NOT NULL,
  `preco_compra` double NOT NULL,
  `preco_venda` double NOT NULL,
  `quantidade` int(11) NOT NULL,
  `tipo` varchar(11) CHARACTER SET utf8 NOT NULL,
  `tamanho` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `estoque`
--

INSERT INTO `estoque` (`id`, `nome`, `preco_compra`, `preco_venda`, `quantidade`, `tipo`, `tamanho`) VALUES
(1, 'Cal?a', 20, 35, 20, 'Jeans', 'M?dio'),
(2, 'blusa', 15, 20, 50, 'regata', 'M');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8 NOT NULL,
  `senha` varchar(255) CHARACTER SET utf8 NOT NULL,
  `tipo` int(11) NOT NULL,
  `last_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `senha`, `tipo`, `last_login`, `created_at`, `update_at`) VALUES
(1, 'Marcelo', '12345', 1, '2019-06-27 13:49:33', '2019-06-27 00:14:50', '2019-06-27 00:14:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas`
--

CREATE TABLE `vendas` (
  `item_fk` varchar(100) NOT NULL,
  `preco_fk` float NOT NULL,
  `codigo_fk` int(11) NOT NULL,
  `preco_total` float NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `autenticacao`
--
ALTER TABLE `autenticacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estoque`
--
ALTER TABLE `estoque`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendas`
--
ALTER TABLE `vendas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codigo_fk` (`codigo_fk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `autenticacao`
--
ALTER TABLE `autenticacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `estoque`
--
ALTER TABLE `estoque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `vendas`
--
ALTER TABLE `vendas`
  ADD CONSTRAINT `vendas_ibfk_1` FOREIGN KEY (`codigo_fk`) REFERENCES `estoque` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
