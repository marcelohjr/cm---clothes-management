<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function login()
	{
		$method = $_SERVER['REQUEST_METHOD'];

		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
			$params = $_REQUEST;
		        
		    $nome = $params['nome'];
		    $senha = $params['senha'];

		        	
		    $response = $this->MyModel->login($nome, $senha);
			echo $response;
			json_output($response['status'],$response);
		}
	}

	public function logout()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
		        $response = $this->MyModel->logout();
				json_output($response['status'],$response);
			
		}
	}
	
}
