<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estoque extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

	public function index()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
		        $response = $this->MyModel->auth();
		        if($response['status'] == 200){
		        	$resp = $this->MyModel->estoque_all();
	    			json_output($response['status'],$resp);
		        }
			
		}
	}

	/*
	public function detail($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
		        $response = $this->MyModel->auth();
		        if($response['status'] == 200){
		        	$resp = $this->MyModel->book_detail_data($id);
					json_output($response['status'],$resp);
		        }
			}
		}
	}
	*/
	public function create()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
				$response = $this->MyModel->auth();
				echo '$response';
		        $respStatus = $response['status'];
		        if($response['status'] == 200){
					$params =$_REQUEST;
					if ($params['nome'] == "" || $params['preco_compra'] == "") {
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Campos não podem estar vazios');
					} else {
		        		$resp = $this->MyModel->estoque_create($params);
					}
					json_output($respStatus,$resp);
		        }
			
			}
	}

	public function update($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'PUT' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
		        $response = $this->MyModel->auth();
		        $respStatus = $response['status'];
		        if($response['status'] == 200){
					$params = $_REQUEST;
					$params['updated_at'] = date('Y-m-d H:i:s');
					if ($params['nome'] == "" || $params['preco_compra'] == "") {
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Campos não podem estar vazios');
					} else {
		        		$resp = $this->MyModel->estoque_update($id,$params);
					}
					json_output($respStatus,$resp);
		        }
			
		}
	}

	public function delete($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'DELETE' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
		        $response = $this->MyModel->auth();
		        if($response['status'] == 200){
		        	$resp = $this->MyModel->estoque_delete($id);
					json_output($response['status'],$resp);
		        }
			}
		}
	}

}
