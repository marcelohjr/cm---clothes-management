<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyModel extends CI_Model {

 

    public function login($nome,$senha)
    {
        $q  = $this->db->select('id, senha')->from('usuario')->where('nome',$nome)->get()->row();
        
        if($q == ""){
            return array('status' => 204,'message' => 'Usuário não encontrado.');
        } else {
            $hashed_password = $q->senha;
            $id              = $q->id;
            //echo $hashed_password ." ".$senha;
            
        //exit;
            if (hash_equals($hashed_password, $senha)) {
               $last_login = date('Y-m-d H:i:s');
               $token = crypt(substr( md5(rand()), 0, 7));
               $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
               $this->db->trans_start();
               $this->db->where('id',$id)->update('usuario',array('last_login' => $last_login));
               $this->db->insert('autenticacao',array('usuario_id' => $id,'token' => $token,'expired_at' => $expired_at));
               if ($this->db->trans_status() === FALSE){
                  $this->db->trans_rollback();
                  return array('status' => 500,'message' => 'Internal server error.');
               } else {
                  $this->db->trans_commit();
                  return array('status' => 200,'message' => 'Successfully login.','id' => $id, 'token' => $token);
               }
            } else {
                echo "Wrong password";
                exit();
               return array('status' => 204,'message' => 'Wrong password.');
            }
        }
    }

    public function logout()
    {
        $users_id  = $this->input->get_request_header('User-ID', TRUE);
        $token     = $this->input->get_request_header('Authorization', TRUE);
        $this->db->where('usuario_id',$users_id)->where('token',$token)->delete('autenticacao');
        return array('status' => 200,'message' => 'Successfully logout.');
    }

    public function auth()
    {
        $users_id  = $this->input->get_request_header('User-ID', TRUE);
        $token     = $this->input->get_request_header('Authorization', TRUE);
        $q  = $this->db->select('expired_at')->from('autenticacao')->where('usuario_id',$users_id)->where('token',$token)->get()->row();
        if($q == ""){
            return json_output(401,array('status' => 401,'message' => 'Não Autorizado.'));
        } else {
            if($q->expired_at < date('Y-m-d H:i:s')){
                return json_output(401,array('status' => 401,'message' => 'Sua sessão expirou.'));
            } else {
                $updated_at = date('Y-m-d H:i:s');
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->where('usuario_id',$users_id)->where('token',$token)->update('autenticacao',array('expired_at' => $expired_at,'update_at' => $updated_at));
                return array('status' => 200,'message' => 'Autorizado.');
            }
        }
    }

    public function estoque_all()
    {
        return $this->db->select('id,nome,preco_compra,preco_venda,quantidade,tipo,tamanho')->from('estoque')->order_by('id','desc')->get()->result();
    }
    /*
    public function book_detail_data($id)
    {
        return $this->db->select('id,title,author')->from('books')->where('id',$id)->order_by('id','desc')->get()->row();
    }
    */
    public function estoque_create($data)
    {
        $this->db->insert('estoque',$data);
        return array('status' => 201,'message' => 'Realizado com sucesso.');
    }
    
    public function estoque_update($id,$data)
    {
        $this->db->where('id',$id)->update('estoque',$data);
        return array('status' => 200,'message' => 'Realizado com sucesso.');
    }

    public function estoque_delete($id)
    {
        $this->db->where('id',$id)->delete('estoque');
        return array('status' => 200,'message' => 'Realizado com sucesso.');
    }

}
